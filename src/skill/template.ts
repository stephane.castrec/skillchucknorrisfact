import { Directive } from "ask-sdk-model";
import { Fact } from "../commons/services";
import  aplDoc from "./apls/apl";

const getAplParam = (imgUrl) => {
    return {
        "bodyTemplate7Data": {
            "type": "object",
            "objectId": "bt7Sample",
            "title": "Chuck Facts",
            "image": {
                "contentDescription": null,
                "smallSourceUrl": null,
                "largeSourceUrl": null,
                "sources": [
                    {
                        "url": imgUrl,
                        "size": "small",
                        "widthPixels": 0,
                        "heightPixels": 0
                    },
                    {
                        "url": imgUrl,
                        "size": "large",
                        "widthPixels": 0,
                        "heightPixels": 0
                    }
                ]
            },
            "logoUrl": "https://chuckfacts.s3-eu-west-1.amazonaws.com/chucknorris_large.png",
        }
    }
}

export const apl = (fact: Fact): Directive => {
    return {
        type: 'Alexa.Presentation.APL.RenderDocument',
        document: aplDoc,
        datasources: getAplParam(fact.fact)
    };
}