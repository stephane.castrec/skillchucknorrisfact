'use strict'
import { HandlerInput } from 'ask-sdk';

export const getApplicationId = (input: HandlerInput): string => {
    console.log('getApplicationId', JSON.stringify(input));
    if (input.requestEnvelope && input.requestEnvelope.session && input.requestEnvelope.session.application) {
        return input.requestEnvelope.session.application.applicationId;
    } else {
        return input.requestEnvelope.context.System.application.applicationId;
    }
}

export const getMediaToken = (input: HandlerInput) => {
    if (input && input.requestEnvelope.context && input.requestEnvelope.context.AudioPlayer) {
        return input.requestEnvelope.context.AudioPlayer.token;
    }
    return null;
}

export const getOffsetInMilliseconds = (handlerInput: HandlerInput) => {
    // Extracting offsetInMilliseconds received in the request.
    if (handlerInput && handlerInput.requestEnvelope.context && handlerInput.requestEnvelope.context.AudioPlayer) {
        return handlerInput.requestEnvelope.context.AudioPlayer.offsetInMilliseconds;
    }
    return null;
}

export const getUserId = (input: HandlerInput): string => {
    console.log('getUserId', JSON.stringify(input));
    if (input.requestEnvelope && input.requestEnvelope.session && input.requestEnvelope.session.user) {
        return input.requestEnvelope.session.user.userId;
    } else {
        return input.requestEnvelope.context.System.user.userId;
    }
}
export const addSpeakBalise = (speech: string) => {
    return `<speak>${speech}</speak>`;
}
export const audioFormat = (url) => {
    return '<audio src="' + url + '"/>';
}
export const getOne = (array) => {
    let i = Math.floor(Math.random() * Math.floor(array.length));
    let msg = array[i];
    return msg;
}
