import { Constants } from './Constants';
import { getFactImg, getFactTxt } from './services.js';
import { HandlerInput } from 'ask-sdk';
import { apl } from '../skill/template';
import { SkillInputUtils } from '../skill/SkillInputUtils';
const Entities = require('html-entities').AllHtmlEntities;


export const playFact = async (input: HandlerInput, isWelcome: boolean) => {
    let msg;
    if (isWelcome) {
        msg = Constants.messages.welcome();
    } else {
        msg = Constants.messages.next();
    }
    try {
        const fact = await getFactTxt();
        console.log("fact txt", fact);
        const question = Constants.messages.reprompt();
        const entities = new Entities();
        const f = entities.decode(fact.fact);
        input.responseBuilder.speak(`${msg} <break time="0.5s"/> ${f} <break time="0.8s"/> ${question}`)
            .reprompt(Constants.messages.reprompt())
    } catch (e) {
        input.responseBuilder.speak(Constants.messages.error())
            .withShouldEndSession(true);
    }
    input.responseBuilder.reprompt(Constants.messages.reprompt())
}   
