import * as request from 'request-promise-native';

const urlImg = 'https://www.chucknorrisfacts.fr/api/get?data=tri:alea;nb:1;type:img';
const urlTxt = 'https://www.chucknorrisfacts.fr/api/get?data=tri:alea;nb:1;type:txt';

/*
[{"id":"271","fact":"http:\/\/www.chucknorrisfacts.fr\/img\/upload\/q48da669cd83d2.jpg","date":"1222288034","vote":"5494","points":"21503"}]
*/
export interface Fact {
    id: string;
    fact: string;
    vote: number;
    points: string;
}

const getFact = async (url: string): Promise<Fact> => {
    var options = {
        uri:url,
        json: true
    };

    const facts = await request.get(options);
    return facts[0];
}

export const getFactImg = async (): Promise<Fact> => {
    return getFact(urlImg);
}

export const getFactTxt = async (): Promise<Fact> => {
    return getFact(urlTxt);
}