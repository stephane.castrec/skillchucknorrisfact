import { ui } from 'ask-sdk-model';
import { getOne } from './utils/utils';


export const Constants = {
    messages: {
        welcome: () => {
            return 'bienvenue sur Chuck Facts.'; 
        },
        next: () => {
            return 'En voilà une autre.';
        },
        error: () => {
            return 'Il semblerait que Chuck est cassé la skill. Ré-essais dans un instant.';
        },
        help: (): string => {
            return "Avec Chuck Facts, riez aux sons de Chuck. Merci à www.chucknorrisfacts.fr pour les facts. Voulez vous une fact maintenant? ?";
        },
        not_supported: () => {
            return "Cette skill ne supporte pas cet appareil";
        },
        reprompt: () => {
            return "Souhaitez vous une autre fact?"
        },
        bye: () => {
            return "Chuck vous dit à bientôt";
        }
    }
}