'use strict';

import 'mocha';
import { assert } from 'chai';
import { getFactTxt } from '../src/commons/services';

describe('Service tests', () => {
    it('getFactText', async () => {
        const fact = await getFactTxt();
        assert.isNotNull(fact);
        assert.isNotNull(fact.fact);
    });
});